# RNTuple examples

Test code to learn more about how to use RNTuple. It requires a rather specific ROOT setup, namely:

```
setupATLAS
lsetup "views LCG_97a x86_64-centos7-gcc8-opt"
source /cvmfs/sft.cern.ch/lcg/releases/ROOT/v6.24.00-6b096/x86_64-centos7-gcc8-opt/bin/thisroot.sh
```

The code can then be compiled and run within ROOT, e.g.

`.L RNTupleExample.C++`

The code is then run with 

`Run()`

The conversion alone (without the example reading) can be done with

`Convert(inputfile, outputfile)`

The name of the input and output files for Run() are in the code itself. The Convert function does, however, take them as input arguments (i.e. used when called from python script):

`python runMakeRNTuple.py <input folder with root files to be converted>`

Before using the python script the RNTupleExample.C must be compiled (as described above). Note also that the paths set in the python script itself would need to be changed so that they reflect the directories on your local system.
