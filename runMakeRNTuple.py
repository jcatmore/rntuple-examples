import os, sys
from os import listdir
from os.path import isfile, join
import ROOT as R
import multiprocessing as mp

mypath = sys.argv[1]

onlyfiles = [f for f in listdir(mypath) if (isfile(join(mypath, f)) and f.endswith(".root"))]

R.gSystem.AddDynamicPath("/home/eirikgr/rntuple-examples")
R.gROOT.ProcessLine(".include /home/eirikgr/rntuple-examples")
R.gInterpreter.AddIncludePath("/storage/shared/software/Input/")                                                                                                                                    
R.gInterpreter.Declare('#include \"/home/eirikgr/rntuple-examples/RNTupleExample.C\"')
#R.gSystem.Load("RNTupleExample_C.so") # Library with the myFilter function"              

newpath = mypath.replace("ATLAS_opendata","ATLAS_opendata/RNTuples")

def doConvert(inpath, outpath):
    R.Convert(inpath,outpath)
    

if not os.path.exists(newpath):
    print("Path %s does not exists"%newpath)
    sys.exit()

n = 1
pids = []
for of in onlyfiles:
    #if not of.endswith(".root"): continue
    if os.path.isfile(newpath+"/"+of):
        print("File %s exists already as RNTuple. Skipping"%of)
        continue
    print("Converting file %s (%i/%i) to RNTuple"%(of,n,len(onlyfiles)))

    p = mp.Process(target=doConvert, args=(mypath+"/"+of,newpath+"/"+of,))
    p.name = str(n)
    print( ">>> Started: "+str(n) )
    pids.append(p)
    p.start()
    
    #R.Convert(mypath+"/"+of,newpath+"/"+of)
    n += 1
    #break
    #print(of)
